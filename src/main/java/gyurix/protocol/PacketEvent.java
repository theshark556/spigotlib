package gyurix.protocol;

import io.netty.channel.Channel;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public abstract class PacketEvent
        extends Event
        implements Cancellable {
    private static final HandlerList hl = new HandlerList();
    protected final Object packet;
    private final Channel channel;
    private final Protocol.NewChannelHandler handler;
    private boolean cancelled;

    public PacketEvent(Channel channel, Protocol.NewChannelHandler handler, Object packet) {
        this.channel = channel;
        this.packet = packet;
        this.handler = handler;
    }

    public static HandlerList getHandlerList() {
        return hl;
    }

    public HandlerList getHandlers() {
        return hl;
    }

    public Channel getChannel() {
        return this.channel;
    }

    public Player getPlayer() {
        return this.handler.player;
    }

    public Object getPacketObject() {
        return this.packet;
    }

    public abstract Object[] getPacketData();

    public /* varargs */ abstract void setPacketData(Object... var1);

    public abstract boolean setPacketData(int var1, Object var2);

    public boolean isCancelled() {
        return this.cancelled;
    }

    public void setCancelled(boolean cancel) {
        this.cancelled = cancel;
    }
}

